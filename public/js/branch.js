const data = [
    {
        data: "check",
        name: "check",
        orderable: false,
        sortable: false,
        searchable: false
    },
    { data: "branch_name", name: "branch_name" },
    { data: "email", name: "email" },
    { data: "telepon", name: "telepon" },
    { data: "address", name: "address" },
    {
        data: "status",
        name: "status",
        orderable: false,
        sortable: false,
        searchable: false
    },
    { data: "created_at", name: "created_at" },
    { data: "btn", name: "btn" }
];

$(function() {
    getAllData();
    Table({ table: "#table", data: data, url: "/master/branch/get" });
    $("#delete").on("click", function() {
        $.ajax({
            url: "/master/branch/delete",
            data: {
                id: value_checkbox
            },
            type: "DELETE",
            headers: {
                "X-CSRF-TOKEN": csrf_token
            },
            success: res => {
                RefreshTable("table");
                getAllData();
            },
            error: err => console.log(err)
        });
    });

    $("#table").on("change", ".status", function() {
        $.ajax({
            url: "/master/branch/change",
            data: {
                id: $(this).data("id"),
                status: $(this).val()
            },
            type: "PUT",
            headers: {
                "X-CSRF-TOKEN": csrf_token
            },
            success: res => {
                RefreshTable("table");
                getAllData();
            },
            error: err => console.log(err)
        });
    });

    $('button[name="total"]').on("click", function() {
        Table({ table: "#table", data: data, url: "/master/branch/get"});
    });

    $('button[name="trashed"]').on("click", function() {
        Table({ table: "#table", data: data, url: "/master/branch/get", parm:{parm:'trashed'}});
    });
    $('button[name="active"]').on("click", function() {
        Table({ table: "#table", data: data, url: "/master/branch/get", parm:{parm:'status', value:1}});
    });
    $('button[name="inactive"]').on("click", function() {
        Table({ table: "#table", data: data, url: "/master/branch/get", parm:{parm:'status', value:0}});
    });
    $("#insert-branch").on("submit", function(e) {
        e.preventDefault();
        $.ajax({
            url: "/master/branch/insert",
            data: new FormData(this),
            processData: false,
            contentType: false,
            type: "POST",
            headers: {
                "X-CSRF-TOKEN": csrf_token
            },
            success: res => {
                RefreshTable("table");
                getAllData();
            },
            error: err => console.log(err)
        });
    });

    $("#update-branch").on("submit", function(e) {
        e.preventDefault();
        $.ajax({
            url: "/master/branch/update",
            data: new FormData(this),
            processData: false,
            contentType: false,
            type: "POST",
            headers: {
                "X-CSRF-TOKEN": csrf_token
            },
            success: res => {
                RefreshTable("table");
                getAllData();
                $("#edit-branch").modal("hide");
            },
            error: err => console.log(err)
        });
    });

    $("#table").on("click", "#edit", function(e) {
        e.preventDefault();
        $.ajax({
            url: "/master/branch/get/" + $(this).data("id"),
            type: "get",
            headers: {
                "X-CSRF-TOKEN": csrf_token
            },
            success: res => {
                $("#edit-branch").modal("show");
                Object.keys(res.data).map(key => {
                    $(`#edit-branch .form-control[name="${key}"]`).val(res.data[key]);
                });
            },
            error: err => console.log(err)
        });
    });

    $("#table").on("click", "#delete", function(e) {
        e.preventDefault();
        $.ajax({
            url: "/master/branch/delete",
            data: {
                id: $(this).data("id")
            },
            type: "DELETE",
            headers: {
                "X-CSRF-TOKEN": csrf_token
            },
            success: res => {
                RefreshTable("table");
                getAllData();
            },
            error: err => console.log(err)
        });
    });

    $("#table").on("click", "#restore", function(e) {
        e.preventDefault();
        $.ajax({
            url: "/master/branch/delete",
            data: {
                id: $(this).data("id"),
                parm : 'restore'
            },
            type: "DELETE",
            headers: {
                "X-CSRF-TOKEN": csrf_token
            },
            success: res => {
                RefreshTable("table");
                getAllData();
            },
            error: err => console.log(err)
        });
    });

    $("#table").on("click", "#deletePermanent", function(e) {
        e.preventDefault();
        $.ajax({
            url: "/master/branch/delete",
            data: {
                id: $(this).data("id"),
                parm : 'deletePermanent'
            },
            type: "DELETE",
            headers: {
                "X-CSRF-TOKEN": csrf_token
            },
            success: res => {
                RefreshTable("table");
                getAllData();
            },
            error: err => console.log(err)
        });
    });
})

function getAllData() {
    $.ajax({
        url: "/master/branch/all",
        headers: {
            "X-CSRF-TOKEN": csrf_token
        },
        success: res => {
            Object.keys(res.data).map(key => {
                $(`span[name="${key}"]`).text(res.data[key]);
            });
        },
        error: err => console.log(err)
    });
}
