// $.noConflict();
const data = [
    {
        data: "check",
        name: "check",
        orderable: false,
        sortable: false,
        searchable: false
    },
    { data: "name", name: "name" },
    { data: "description", name: "description" },
    {
        data: "status",
        name: "status",
        orderable: false,
        sortable: false,
        searchable: false
    },
    { data: "created_at", name: "created_at" },
    { data: "btn", name: "btn" }
];

$(function() {
    getAllData();
    Table({ table: "#table", data: data, url: "/master/role/get" });

    $("#delete").on("click", function() {
        $.ajax({
            url: "/master/role/delete",
            data: {
                id: value_checkbox
            },
            type: "DELETE",
            headers: {
                "X-CSRF-TOKEN": csrf_token
            },
            success: res => {
                RefreshTable("role");
                getAllData();
            },
            error: err => console.log(err)
        });
    });

    $("#table").on("change", ".status", function() {
        $.ajax({
            url: "/master/role/change",
            data: {
                id: $(this).data("id"),
                status: $(this).val()
            },
            type: "PUT",
            headers: {
                "X-CSRF-TOKEN": csrf_token
            },
            success: res => {
                RefreshTable("role");
                getAllData();
            },
            error: err => console.log(err)
        });
    });

    $('button[name="total"]').on("click", function() {
        Table({ table: "#table", data: data, url: "/master/role/get"});
    });

    $('button[name="trashed"]').on("click", function() {
        Table({ table: "#table", data: data, url: "/master/role/get", parm:{parm:'trashed'}});
    });
    $('button[name="active"]').on("click", function() {
        Table({ table: "#table", data: data, url: "/master/role/get", parm:{parm:'status', value:1}});
    });
    $('button[name="inactive"]').on("click", function() {
        Table({ table: "#table", data: data, url: "/master/role/get", parm:{parm:'status', value:0}});
    });
});

$("#insert-role").on("submit", function(e) {
    e.preventDefault();
    $.ajax({
        url: "/master/role/insert",
        data: new FormData(this),
        processData: false,
        contentType: false,
        type: "POST",
        headers: {
            "X-CSRF-TOKEN": csrf_token
        },
        success: res => {
            RefreshTable("role");
            getAllData();
        },
        error: err => console.log(err)
    });
});

$("#update-role").on("submit", function(e) {
    e.preventDefault();
    $.ajax({
        url: "/master/role/update",
        data: new FormData(this),
        processData: false,
        contentType: false,
        type: "POST",
        headers: {
            "X-CSRF-TOKEN": csrf_token
        },
        success: res => {
            RefreshTable("role");
            getAllData();
            $("#edit-role").modal("hide");
        },
        error: err => console.log(err)
    });
});

$("#table").on("click", "#edit", function(e) {
    e.preventDefault();
    $.ajax({
        url: "/master/role/get/" + $(this).data("id"),
        type: "get",
        headers: {
            "X-CSRF-TOKEN": csrf_token
        },
        success: res => {
            $("#edit-role").modal("show");
            Object.keys(res.data).map(key => {
                $(`#edit-role .form-control[name="${key}"]`).val(res.data[key]);
            });
        },
        error: err => console.log(err)
    });
});

$("#table").on("click", "#delete", function(e) {
    e.preventDefault();
    $.ajax({
        url: "/master/role/delete",
        data: {
            id: $(this).data("id")
        },
        type: "DELETE",
        headers: {
            "X-CSRF-TOKEN": csrf_token
        },
        success: res => {
            RefreshTable("role");
            getAllData();
        },
        error: err => console.log(err)
    });
});

$("#table").on("click", "#restore", function(e) {
    e.preventDefault();
    $.ajax({
        url: "/master/role/delete",
        data: {
            id: $(this).data("id"),
            parm : 'restore'
        },
        type: "DELETE",
        headers: {
            "X-CSRF-TOKEN": csrf_token
        },
        success: res => {
            RefreshTable("role");
            getAllData();
        },
        error: err => console.log(err)
    });
});

$("#table").on("click", "#deletePermanent", function(e) {
    e.preventDefault();
    $.ajax({
        url: "/master/role/delete",
        data: {
            id: $(this).data("id"),
            parm : 'deletePermanent'
        },
        type: "DELETE",
        headers: {
            "X-CSRF-TOKEN": csrf_token
        },
        success: res => {
            RefreshTable("role");
            getAllData();
        },
        error: err => console.log(err)
    });
});

function getAllData() {
    $.ajax({
        url: "/master/role/all",
        headers: {
            "X-CSRF-TOKEN": csrf_token
        },
        success: res => {
            Object.keys(res.data).map(key => {
                $(`span[name="${key}"]`).text(res.data[key]);
            });
        },
        error: err => console.log(err)
    });
}
