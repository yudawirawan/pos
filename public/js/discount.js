// $.noConflict();
const data = [
    {
        data: "check",
        name: "check",
        orderable: false,
        sortable: false,
        searchable: false
    },
    { data: "item.item_name", name: "item.item_name" },
    { data: "unit.name_unit", name: "unit.name_unit" },
    { data: "discount_type.name", name: "discount_type.name" },
    { data: "start_date", name: "start_date" },
    { data: "end_date", name: "end_date" },
    { data: "user.name", name: "user.name" },
    { data: "created_at", name: "created_at" },
    { data: "btn", name: "btn" }
];

$(function() {
    getAllData();
    Table({ table: "#table", data: data, url: "/master/item/discount/get" });

    $("#delete").on("click", function() {
        $.ajax({
            url: "/master/item/discount/delete",
            data: {
                id: value_checkbox
            },
            type: "DELETE",
            headers: {
                "X-CSRF-TOKEN": csrf_token
            },
            success: res => {
                Table({ table: "#table", data: data, url: "/master/item/discount/get"});
                getAllData();
            },
            error: err => console.log(err)
        });
    });


    $('button[name="total"]').on("click", function() {
        Table({ table: "#table", data: data, url: "/master/item/discount/get"});
    });
    $('button[name="trashed"]').on("click", function() {
        Table({ table: "#table", data: data, url: "/master/item/discount/get", parm:{parm:'trashed'}});
    });
});

$("#insert-discount").on("submit", function(e) {
    e.preventDefault();
    $.ajax({
        url: "/master/item/discount/add",
        data: new FormData(this),
        processData: false,
        contentType: false,
        type: "POST",
        headers: {
            "X-CSRF-TOKEN": csrf_token
        },
        beforeSend: function(){
            $('#load').html("Loading...");
            $('#load').prop("disabled", true);
        },
        success: res => {
            location.reload(true);
        },
        error: err => console.log(err)
    });
});

$("#update-discount").on("submit", function(e) {
    e.preventDefault();
    $.ajax({
        url: "/master/item/discount/update",
        data: new FormData(this),
        processData: false,
        contentType: false,
        type: "POST",
        headers: {
            "X-CSRF-TOKEN": csrf_token
        },
        success: res => {
            RefreshTable("table");
            getAllData();
            $('#edit-discount').modal('hide');
            $('.modal-backdrop').remove();
        },
        error: err => console.log(err)
    });
});

$("#table").on("click", "#edit", function(e) {
    e.preventDefault();
    $.ajax({
        url: "/master/item/discount/get/" + $(this).data("id"),
        type: "get",
        headers: {
            "X-CSRF-TOKEN": csrf_token
        },
        success: res => {
            $("#edit-discount").modal("show");
            Object.keys(res.data).map(key => {
                $(`#edit-discount .form-control[name="${key}"]`).val(res.data[key]);
            });
        },
        error: err => console.log(err)
    });
});

$("#table").on("click", "#delete", function(e) {
    e.preventDefault();
    $.ajax({
        url: "/master/item/discount/delete",
        data: {
            id: $(this).data("id")
        },
        type: "DELETE",
        headers: {
            "X-CSRF-TOKEN": csrf_token
        },
        success: res => {
            Table({ table: "#table", data: data, url: "/master/item/discount/get"});
            //RefreshTable("item_group");
            getAllData();
        },
        error: err => console.log(err)
    });
});

$("#table").on("click", "#restore", function(e) {
    e.preventDefault();
    $.ajax({
        url: "/master/item/discount/delete",
        data: {
            id: $(this).data("id"),
            parm : 'restore'
        },
        type: "DELETE",
        headers: {
            "X-CSRF-TOKEN": csrf_token
        },
        success: res => {
            Table({ table: "#table", data: data, url: "/master/item/discount/get", parm:{parm:'trashed'}});
            //RefreshTable("role");
            getAllData();
        },
        error: err => console.log(err)
    });
});

$("#table").on("click", "#deletePermanent", function(e) {
    e.preventDefault();
    $.ajax({
        url: "/master/item/discount/delete",
        data: {
            id: $(this).data("id"),
            parm : 'deletePermanent'
        },
        type: "DELETE",
        headers: {
            "X-CSRF-TOKEN": csrf_token
        },
        success: res => {
            Table({ table: "#table", data: data, url: "/master/item/discount/get", parm:{parm:'trashed'}});
            getAllData();
        },
        error: err => console.log(err)
    });
});

function getAllData() {
    $.ajax({
        url: "/master/item/discount/all",
        headers: {
            "X-CSRF-TOKEN": csrf_token
        },
        success: res => {
            Object.keys(res.data).map(key => {
                $(`span[name="${key}"]`).text(res.data[key]);
            });
        },
        error: err => console.log(err)
    });
}
