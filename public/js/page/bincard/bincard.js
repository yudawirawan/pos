const data = [
    {
        data: "check",
        name: "check",
        orderable: false,
        sortable: false,
        searchable: false
    },
    { data: 'invoice', name: 'invoice' },
    { data: 'bintype', name: 'bintype' },
    {
        data: "status",
        name: "status",
        orderable: false,
        sortable: false,
        searchable: false
    },
    {
        data: "btn",
        name: "btn",
        orderable: false,
        sortable: false,
        searchable: false
    }
];
$(function () {
    // var x;
    // x = $("#deleteper").detach();
    $('#deleteper').hide(100);
    // var cacheDom = "";
    // cacheDom = $('#deleteper');
    // $('#deleteper').remove();

    // var backUpReplaced;
    // backUpReplaced = $('#deleteper').replaceWith('');
    // var del = $('.deleteper').detach();
    toggledeleteAllBtn()
    getAllData();
    Table({ table: "#table", data: data, url: "/master/bincard/get" });

    $("#table").on("change", ".status", function () {
        $.ajax({
            url: "/master/bincard/change",
            data: {
                id: $(this).data("id"),
                status: $(this).val()
            },
            type: "PUT",
            headers: {
                "X-CSRF-TOKEN": csrf_token
            },
            success: res => {
                RefreshTable("table");
                getAllData();
            },
            error: err => console.log(err)
        });
    });

    $('#createNewCustomer').click(function () {
        $('#saveBtn').val("create-Customer");
        $('#Bincard_id').val('');
        $('#CustomerForm').trigger("reset");
        $('#modelHeading').html("Create New Customer");
        $('#ajaxModel').modal('show');
    });
    $('body').on('click', '.editCustomer', function () {
        var Bincard_id = $(this).data('id');
        $.get("" + '/master/bincard/crud/' + Bincard_id + '/edit', function (data) {
            $('#modelHeading').html("Edit Customer");
            $('#saveBtn').val("edit-user");
            $('#ajaxModel').modal('show');
            $('#Bincard_id').val(data.id);
            $('#invoice').val(data.invoice);
            $('#bintype').val(data.bintype);
            $('#item_id').val(data.item_id);
            $('#qty').val(data.qty);
            $('#price').val(data.price);
            $('#amount_price').val(data.amount_price);
            $('#uom_code').val(data.uom_code);
            $('#memo').val(data.memo);
        })
    });
    $('#saveBtn').click(function (e) {
        e.preventDefault();
        $.ajax({
            data: $('#CustomerForm').serialize(),
            url: "",
            type: "POST",
            headers: {
                "X-CSRF-TOKEN": csrf_token
            },
            dataType: 'json',
            beforeSend: function () {
                $('#CustomerForm').trigger("reset");
                $('#saveBtn').html("Loading...");
                $('#saveBtn').prop("disabled", true);
            },
            success: function (data) {
                $('#saveBtn').prop("disabled", false);
                $('#saveBtn').html("Save changes");
                $('#CustomerForm').trigger("reset");
                $('#ajaxModel').modal('hide');
                RefreshTable("table");
                getAllData();
                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": true,
                    "positionClass": "toast-bottom-right",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "2000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                }
                toastr.success('Data Berhasil Di Hapus !', 'Konversi Satuan')
            },
            error: function (data) {
                console.log('Error:', data);
                $('#saveBtn').html('Save Changes');
            }
        });
    });

    $('button[name="total"]').on("click", function () {
        Table({ table: "#table", data: data, url: "/master/bincard/get" });
        $('#deleteper').hide();
        $('#delete').show();
    });

    $('button[name="trashed"]').on("click", function () {
        Table({ table: "#table", data: data, url: "/master/bincard/get", parm: { parm: 'trashed' } });
        // $(".hapus").prepend(x);
        // $('.hapus').append(backUpReplaced);
        // del.appendTo('.hapus');
        $('#deleteper').show();
        $('#delete').hide();
        // toggledeleteAllBtnn();
    });
    $('button[name="active"]').on("click", function () {
        Table({ table: "#table", data: data, url: "/master/bincard/get", parm: { parm: 'status', value: 1 } });
        $('#deleteper').hide();
        $('#delete').show();
    });
    $('button[name="inactive"]').on("click", function () {
        Table({ table: "#table", data: data, url: "/master/bincard/get", parm: { parm: 'status', value: 0 } });
        $('#deleteper').hide();
        $('#delete').show();
    });

    $("#deleteper").on("click", function () {
        swal({
            title: 'Yakin hapus data ?',
            text: 'Data yang udah di hapus gak bisa di balikin!',
            icon: 'warning',
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    // swal('Poof! Your imaginary file has been deleted!', {
                    //     icon: 'success',
                    // });
                    $.ajax({
                        url: "/master/bincard/delete",
                        data: {
                            id: value_checkbox,
                            parm: "withTrashed"
                        },
                        type: "DELETE",
                        headers: {
                            "X-CSRF-TOKEN": csrf_token
                        },
                        success: res => {
                            RefreshTable("table");
                            getAllData();
                            toastr.options = {
                                "closeButton": true,
                                "debug": false,
                                "newestOnTop": false,
                                "progressBar": true,
                                "positionClass": "toast-bottom-right",
                                "preventDuplicates": false,
                                "onclick": null,
                                "showDuration": "300",
                                "hideDuration": "1000",
                                "timeOut": "2000",
                                "extendedTimeOut": "1000",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut"
                            }
                            toastr.success('Data Berhasil Di Hapus !', 'Konversi Satuan')
                        },
                        error: err => console.log(err)
                    });
                } else {
                    // swal('Your imaginary file is safe!');
                }
            });
    });

    $("#delete").on("click", function () {
        swal({
            title: 'Yakin hapus data ?',
            text: 'Data yang udah di hapus gak bisa di balikin!',
            icon: 'warning',
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    // swal('Poof! Your imaginary file has been deleted!', {
                    //     icon: 'success',
                    // });
                    $.ajax({
                        url: "/master/bincard/delete",
                        data: {
                            id: value_checkbox,
                        },
                        type: "DELETE",
                        headers: {
                            "X-CSRF-TOKEN": csrf_token
                        },
                        success: res => {
                            RefreshTable("table");
                            getAllData();
                            $('button.delete').addClass('d-none');
                            toastr.options = {
                                "closeButton": true,
                                "debug": false,
                                "newestOnTop": false,
                                "progressBar": true,
                                "positionClass": "toast-bottom-right",
                                "preventDuplicates": false,
                                "onclick": null,
                                "showDuration": "300",
                                "hideDuration": "1000",
                                "timeOut": "2000",
                                "extendedTimeOut": "1000",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut"
                            }
                            toastr.success('Data Berhasil Di Hapus !', 'Konversi Satuan')
                        },
                        error: err => console.log(err)
                    });
                } else {
                    // swal('Your imaginary file is safe!');
                }
            });
    });

    $("#table").on("click", "#delete", function (e) {
        e.preventDefault();
        swal({
            title: 'Yakin hapus data ?',
            text: 'Data yang udah di hapus gak bisa di balikin!',
            icon: 'warning',
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    // swal('Poof! Your imaginary file has been deleted!', {
                    //     icon: 'success',
                    // });
                    $.ajax({
                        url: "/master/bincard/delete",
                        data: {
                            id: $(this).data("id")
                        },
                        type: "DELETE",
                        headers: {
                            "X-CSRF-TOKEN": csrf_token
                        },
                        success: res => {
                            RefreshTable("table");
                            getAllData();
                            toastr.options = {
                                "closeButton": true,
                                "debug": false,
                                "newestOnTop": false,
                                "progressBar": true,
                                "positionClass": "toast-bottom-right",
                                "preventDuplicates": false,
                                "onclick": null,
                                "showDuration": "300",
                                "hideDuration": "1000",
                                "timeOut": "2000",
                                "extendedTimeOut": "1000",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut"
                            }
                            toastr.success('Data Berhasil Di Hapus !', 'Konversi Satuan')
                        },
                        error: err => console.log(err)
                    });
                } else {
                    // swal('Your imaginary file is safe!');
                }
            });
    });

    $("#table").on("click", "#restore", function (e) {
        e.preventDefault();
        $.ajax({
            url: "/master/bincard/delete",
            data: {
                id: $(this).data("id"),
                parm: 'restore'
            },
            type: "DELETE",
            headers: {
                "X-CSRF-TOKEN": csrf_token
            },
            success: res => {
                RefreshTable("table");
                getAllData();
            },
            error: err => console.log(err)
        });
    });

    $("#table").on("click", "#deletePermanent", function (e) {
        e.preventDefault();
        swal({
            title: 'Yakin hapus data ?',
            text: 'Data yang udah di hapus gak bisa di balikin!',
            icon: 'warning',
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    // swal('Poof! Your imaginary file has been deleted!', {
                    //     icon: 'success',
                    // });
                    $.ajax({
                        url: `/master/bincard/delete`,
                        data: {
                            id: $(this).data("id"),
                            parm: 'deletePermanent'
                        },
                        type: "DELETE",
                        headers: {
                            "X-CSRF-TOKEN": csrf_token
                        },
                        success: res => {
                            RefreshTable("table");
                            getAllData();
                            toastr.options = {
                                "closeButton": true,
                                "debug": false,
                                "newestOnTop": false,
                                "progressBar": true,
                                "positionClass": "toast-bottom-right",
                                "preventDuplicates": false,
                                "onclick": null,
                                "showDuration": "300",
                                "hideDuration": "1000",
                                "timeOut": "2000",
                                "extendedTimeOut": "1000",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut"
                            }
                            toastr.success('Data Berhasil Di Hapus !', 'Konversi Satuan')
                        },
                        error: err => console.log(err)
                    });
                } else {
                    // swal('Your imaginary file is safe!');
                }
            });
    });
});

$(document).on('click', 'input[name="main_checkbox"]', function () {
    if (this.checked) {
        $('input[name="checkbox-item"]').each(function () {
            this.checked = true;
        });
    } else {
        $('input[name="checkbox-item"]').each(function () {
            this.checked = false;
        });
    }
    toggledeleteAllBtn();
    toggledeleteAllBtnn();
})

$(document).on('change', 'input[name="checkbox-item"]', function () {
    if ($('input[name="checkbox-item"]').length == $('input[name="checkbox-item"]:checked').length) {
        $('input[name="main_checkbox"]').prop('checked', true);
    } else {
        $('input[name="main_checkbox"]').prop('checked', false);
    }
    toggledeleteAllBtn();
    toggledeleteAllBtnn();
});

function toggledeleteAllBtn() {
    if ($('input[name="checkbox-item"]:checked').length > 0) {
        $('button.delete').text('Delete (' + $('input[name="checkbox-item"]:checked').length + ')').removeClass('d-none');
    } else {
        $('button.delete').addClass('d-none');
    }
}

function toggledeleteAllBtnn() {
    if ($('input[name="checkbox-item"]:checked').length > 0) {
        $('button#deleteper').text('Delete Permanent (' + $('input[name="checkbox-item"]:checked').length + ')').removeClass('d-none');
    } else {
        $('button.deleteper').addClass('d-none');
    }
}

function getAllData() {
    $.ajax({
        url: "/master/bincard/all",
        headers: {
            "X-CSRF-TOKEN": csrf_token
        },
        success: res => {
            Object.keys(res.data).map(key => {
                $(`span[name="${key}"]`).text(res.data[key]);
            });
        },
        error: err => console.log(err)
    });
}