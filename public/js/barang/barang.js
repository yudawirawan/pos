$.noConflict();

$(function () {
    console.log("ok")

    $("#barang").DataTable()

    $(".btn-edit").on("click", function () {
        // console.log($(this).data("id"))
        let id = $(this).data('id')
        $.ajax({
            url: `/master/item/${id}/edit`,
            method: "GET",
            success: function (data) {
                $('#modal-edit').find('.modal-body').html(data)
                $('#modal-edit').modal('show')
            },
            error: function (error) {
                console.log(error);
            }
        })
    })

    $(".btn-update").on("click", function () {
        // console.log($(this).data("id"))
        let id = $('#form-edit').find('#id_data').val()
        let formData = $('#form-edit').serialize()
        console.log(formData)
        $.ajax({
            url: `/master/item/${id}`,
            method: "post",
            data: formData,
            success: function (data) {
                // $('#modal-edit').find('.modal-body').html(data)
                // $('#modal-edit').modal('show')
                $("#modal-edit").modal('hide')
                window.location.assign('/master/item')
            },
            error: function (error) {
                console.log(error);
            }
        })
    })

})

$(function (e) {
    $("#chkCheckAll").click(function () {
        $(".checkBoxClass").prop('checked', $(this).prop('checked'));
    })

    $('#deleteAllSelectedRecords').click(function (e) {
        e.preventDefault();
        var allids = []
        $("input:checkbox[name=ids]:checked").each(function () {
            allids.push($(this).val())
        })
        $.ajax({
            url: `/item/selected-item`,
            type: 'DELETE',
            data: {
                ids: allids,
                _token: $("input[name=_token]").val()
            },
            success: function (response) {
                $.each(allids, function (key, val) {
                    $('#sid' + val).remove()
                })
                window.location.assign('/item')
            }
        })
    })
})
