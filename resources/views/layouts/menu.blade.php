<li class="nav-item">
    <a href="#" class="nav-link">
        <i class="nav-icon far fa-copyright"></i>
        <p>Merk</p>
    </a>
</li>

<li class="nav-item">
    <a href="#" class="nav-link">
        <i class="nav-icon far fa-file-alt"></i>
    <p>
        Satuan
        <i class="right fas fa-angle-left"></i>
    </p>
    </a>
<ul class="nav nav-treeview">
    <li class="nav-item">
        <a href="{{ route('unit') }}" class="nav-link">
            <i class="nav-icon far fa-file-alt"></i>
            <p>Satuan</p>
        </a>
    </li>
    <li class="nav-item">
        <a href="{{ route('konversi') }}" class="nav-link">
            <i class="nav-icon far fa-file-alt"></i>
                <p>Konversi Satuan</p>
        </a>
    </li>
</ul>
</li>

<li class="nav-item">
    <a href="#" class="nav-link">
        <i class="nav-icon far fa-file-alt"></i>
    <p>
        Supplier
        <i class="right fas fa-angle-left"></i>
    </p>
    </a>
<ul class="nav nav-treeview">
    <li class="nav-item">
        <a href="{{ route('suppliertype') }}" class="nav-link">
            <i class="nav-icon far fa-file-alt"></i>
            <p>Supplier Type</p>
        </a>
    </li>
    <li class="nav-item">
        <a href="{{ route('supplier') }}" class="nav-link">
            <i class="nav-icon far fa-file-alt"></i>
                <p>Supplier</p>
        </a>
    </li>
</ul>
</li>

<li class="nav-item">
    <a href="#" class="nav-link">
        <i class="nav-icon far fa-file-alt"></i>
    <p>
        Customer
        <i class="right fas fa-angle-left"></i>
    </p>
    </a>
<ul class="nav nav-treeview">
    <li class="nav-item">
        <a href="{{ route('customertype') }}" class="nav-link">
        <i class="nav-icon fas fa-user-edit"></i>
            <p>Customer Type</p>
        </a>
    </li>
    <li class="nav-item">
        <a href="{{ route('customer') }}" class="nav-link">
       <i class="nav-icon fas fa-users"></i>
                <p>Customer</p>
        </a>
    </li>
</ul>
</li>

<li class="nav-item">
    <a href="{{ route('division') }}" class="nav-link">
        <i class="nav-icon far fa-clone"></i>
        <p>Division</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('shift') }}" class="nav-link">
        <i class="nav-icon far fa-clone"></i>
        <p>Shift</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('color') }}" class="nav-link">
        <i class="nav-icon fas fa-palette"></i>
        <p>Color</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('table') }}" class="nav-link">
        <i class="nav-icon fas fa-table"></i>
        <p>Table</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('majors') }}" class="nav-link">
        <i class="nav-icon fas fa-table"></i>
        <p>Majors</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('typepayment') }}" class="nav-link">
       <i class="nav-icon fas fa-money-bill-wave"></i>
        <p>Type Payment</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('clasification') }}" class="nav-link">
       <i class="nav-icon fab fa-buffer"></i>
        <p>Clasification</p>
    </a>
</li>

<li class="nav-item">
    <a href="/master/bincard/crud" class="nav-link">
       <i class="nav-icon fab fa-buffer"></i>
        <p>Bincard</p>
    </a>
</li>


<li class="nav-item">
    <a href="gallery.html" class="nav-link">
        <i class="nav-icon far fa-image"></i>
        <p>Gallery</p>
    </a>
</li>

<li class="nav-item ">
    <a class="nav-link" data-toggle="collapse" href="#ui-basic1" aria-expanded="false" aria-controls="ui-basic1">
        <span class="menu-title"><i class="nav-icon fas fa-cubes"></i> Master Barang</span>
            <i class="menu-arrow"></i>
            <i class="mdi mdi mdi-table-large menu-icon"></i>
    </a>
        <div class="collapse" id="ui-basic1">
            <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link {{ Request::path() === 'master/item' ? 'active' : '' }}" href="{{ route ('item')}}">&emsp;&emsp;<i class="nav-icon fas fa-circle text-sm"></i>Barang</a></li>
                <li class="nav-item"> <a class="nav-link {{ Request::path() === 'master/item/group' ? 'active' : '' }}" href="{{ route ('item-group')}}">&emsp;&emsp;<i class="nav-icon fas fa-circle text-sm"></i>Kelompok</a></li>
                <li class="nav-item"> <a class="nav-link {{ Request::path() === 'master/item/price' ? 'active' : '' }}" href="{{ route ('item-price')}}">&emsp;&emsp;<i class="nav-icon fas fa-circle text-sm"></i>Harga</a></li>
                <li class="nav-item"> <a class="nav-link {{ Request::path() === 'master/item/discount' ? 'active' : '' }}" href="{{ route ('discount')}}">&emsp;&emsp;<i class="nav-icon fas fa-circle text-sm"></i>Diskon</a></li>
                <li class="nav-item"> <a class="nav-link {{ Request::path() === 'master/item/discount/type' ? 'active' : '' }}" href="{{ route ('discount-type')}}">&emsp;&emsp;<i class="nav-icon fas fa-circle text-sm"></i>Tipe Diskon</a></li>
            </ul>
        </div>
</li>

<li class="nav-item ">
    <a class="nav-link" data-toggle="collapse" href="#ui-basic2" aria-expanded="false" aria-controls="ui-basic1">
        <span class="menu-title"><i class="nav-icon fas fa-rocket"></i> Production</span>
            <i class="menu-arrow"></i>
            <i class="mdi mdi mdi-table-large menu-icon"></i>
    </a>
        <div class="collapse" id="ui-basic2">
            <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link {{ Request::path() === 'master/production' ? 'active' : '' }}" href="{{ route ('production')}}">&emsp;&emsp;<i class="nav-icon fas fa-circle text-sm"></i>Production</a></li>
                <li class="nav-item"> <a class="nav-link {{ Request::path() === 'master/production/detail' ? 'active' : '' }}" href="{{ route ('production-detail')}}">&emsp;&emsp;<i class="nav-icon fas fa-circle text-sm"></i>Details</a></li>
            </ul>
        </div>
</li>

<li class="nav-item ">
    <a class="nav-link" data-toggle="collapse" href="#ui-basic3" aria-expanded="false" aria-controls="ui-basic1">
        <span class="menu-title"><i class="nav-icon fas fa-wrench"></i> Recipe</span>
            <i class="menu-arrow"></i>
            <i class="mdi mdi mdi-table-large menu-icon"></i>
    </a>
        <div class="collapse" id="ui-basic3">
            <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link {{ Request::path() === 'master/recipe' ? 'active' : '' }}" href="{{ route ('recipe')}}">&emsp;&emsp;<i class="nav-icon fas fa-circle text-sm"></i>Recipe</a></li>
                <li class="nav-item"> <a class="nav-link {{ Request::path() === 'master/recipe/detail' ? 'active' : '' }}" href="{{ route ('recipe-detail')}}">&emsp;&emsp;<i class="nav-icon fas fa-circle text-sm"></i>Details</a></li>
            </ul>
        </div>
</li>

<li class="nav-item">
    <a href="{{ route('item-warehouse') }}" class="nav-link">
        <i class="nav-icon fas fa-th"></i>
        <p>Gudang</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('role') }}" class="nav-link">
        <i class="nav-icon fas fa-user-cog"></i>
        <p>Role</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('branch') }}" class="nav-link">
        <i class="nav-icon fas fa-code-branch"></i>
        <p>Cabang</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('user') }}" class="nav-link">
        <i class="nav-icon fas fa-users"></i>
        <p>User</p>
    </a>
</li>
