<div class="form-group">
    <input type="hidden" name="id" value="{{ $konversis->id }}" id="id_data">
    <label for="name">Nama Satuan</label>
    <select class="form-control" name="unit_id">
    @foreach($satuans as $satuan)
        <option value="{{ $satuan->id }}" <?php if($konversis->unit_id == $satuan->id){ echo 'selected';} ?>>{{ $satuan->name_unit }}</option>
    @endforeach
    </select>
</div>
<div class="form-group">
    <label for="id_role">Satuan Baru</label>
    <input type="text" class="form-control" name="satuan_baru" placeholder="Satuan Baru" value="{{ $konversis->satuan_baru }}" required>
</div>