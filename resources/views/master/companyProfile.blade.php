@extends('layouts.app')

@once
@push('page_css')
<link rel="stylesheet" href="{{ asset('css/dataTables/dataTables.bootstrap4.min.css') }}">
@endpush
@endonce

@section('content')

    <div class="container-fluid pt-4">
        <h2 class="text-black-10">Company Profile
        </h2>

        <div class="row">
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box">
                    <button style="border: none" name="total" class="info-box-icon bg-primary elevation-1"><i class="fas fa-user-cog"></i></button>

                    <div class="info-box-content">
                        <span class="info-box-text">Total</span>
                        <span class="info-box-number" name="all">0
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box mb-3">
                    <button style="border: none" name="active" class="info-box-icon bg-success elevation-1"><i class="fas fa-thumbs-up"></i></button>

                    <div class="info-box-content">
                        <span class="info-box-text">Aktif</span>
                        <span class="info-box-number" name="active">0</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->

            <!-- fix for small devices only -->
            <div class="clearfix hidden-md-up"></div>

            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box mb-3">
                    <button style="border: none" name="inactive" class="info-box-icon bg-danger elevation-1"><i class="fas fa-thumbs-down"></i></button>

                    <div class="info-box-content">
                        <span class="info-box-text">Tidak Aktif</span>
                        <span class="info-box-number" name="inactive">0</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box mb-3">
                    <button style="border: none" name="trashed" class="info-box-icon bg-info elevation-1"><i class="fas fa-recycle"></i></button>

                    <div class="info-box-content">
                        <span class="info-box-text">Recycle</span>
                        <span class="info-box-number" name="trashed">0</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Data Company Profile
                        </h3>

                        <div class="card-tools">
                            <div class="input-group input-group-sm">
                                <button class="mr-2 float-right btn btn-primary btn-sm" data-toggle="modal"
                                    data-target="#add-company-profile" ><i class="fas fa-plus"></i> Add</button>
                                <button class="mr-2 float-right btn btn-danger btn-sm" id="delete"><i
                                        class="fas fa-trash"></i>
                                    Delete</button>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body table-responsive p-2">
                        <table class="table table-head-fixed text-nowrap" id="table">
                            <thead>
                                <tr>
                                    <th>
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox" id="customCheckbox"
                                                onchange="checkbox_all(this)">
                                            <label for="customCheckbox" class="custom-control-label"></label>
                                        </div>
                                    </th>
                                    <th>Name</th>
                                    <th>Bussiness Type</th>
                                    <th>Address 1</th>
                                    <th>Telepon 1</th>
                                    <th>Created By</th>
                                    <th>Status</th>
                                    <th>Created At</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
    </div>

    <!-- insert data -->
    <div class="modal fade" id="add-company-profile" aria-modal="true" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="" id="insert-company-profile">
                    <div class="modal-header">
                        <h4 class="modal-title">Add Company Profile</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="name">Name <i class="text-danger">*</i></label>
                            <input required type="text" class="form-control" name="name" placeholder="Enter Company Name">
                        </div>
                        <div class="form-group">
                            <label for="bussiness_type">Bussiness Type <i class="text-danger">*</i></label>
                            <input required type="text" class="form-control" name="bussiness_type" placeholder="Enter Bussiness Type">
                        </div>
                        <div class="form-group">
                            <label for="npwp_number">NPWP Number <i class="text-danger">*</i></label>
                            <input required type="number" class="form-control" name="npwp_number" placeholder="Enter NPWP Number">
                        </div>
                        <div class="form-group">
                            <label for="address_1">Adress 1 <i class="text-danger">*</i></label>
                            <input required type="text" class="form-control" name="address_1" placeholder="Enter Adress 1">
                        </div>
                        <div class="form-group">
                            <label for="address_2">Address 2</i></label>
                            <input type="text" class="form-control" name="address_2" placeholder="Enter Address 2">
                        </div>
                        <div class="form-group">
                            <label for="telepon_1">Telepon 1 <i class="text-danger">*</i></label>
                            <input required type="number" class="form-control" name="telepon_1" placeholder="Enter Telepon 1">
                        </div>
                        <div class="form-group">
                            <label for="telepon_2">Telepon 2 </label>
                            <input type="number" class="form-control" name="telepon_2" placeholder="Enter Telepon 2">
                        </div>
                        <div class="form-group">
                            <label for="fax">Fax <i class="text-danger">*</i></label>
                            <input required type="text" class="form-control" name="fax" placeholder="Enter Fax">
                        </div>
                        <div class="form-group">
                            <label for="bank_name">Bank Name <i class="text-danger">*</i></label>
                            <input required type="text" class="form-control" name="bank_name" placeholder="Enter Bank Name">
                        </div>
                        <div class="form-group">
                            <label for="bank_rekening_number">Bank Account Number <i class="text-danger">*</i></label>
                            <input required type="text" class="form-control" name="bank_rekening_number" placeholder="Enter Bank Account Number">
                        </div>
                        <div class="form-group">
                            <label for="bank_owner">Bank Owner <i class="text-danger">*</i></label>
                            <input required type="text" class="form-control" name="bank_owner" placeholder="Enter Bank Owner">
                        </div>
                        <div class="form-group">
                            <label for="country_id">Country <i class="text-danger">*</i></label>
                            <input required type="text" class="form-control" name="country_id" placeholder="Enter Country">
                        </div>
                        <div class="form-group">
                            <label for="province_id">Province <i class="text-danger">*</i></label>
                            <input required type="text" class="form-control" name="province_id" placeholder="Enter Province">
                        </div>
                        <div class="form-group">
                            <label for="city_id">City <i class="text-danger">*</i></label>
                            <input required type="text" class="form-control" name="city_id" placeholder="Enter City">
                        </div>
                        <div class="form-group">
                            <label for="district_id">District</i></label>
                            <input type="text" class="form-control" name="district_id" placeholder="Enter District">
                        </div>
                        <div class="form-group">
                            <label for="village_id">Village</i></label>
                            <input type="text" class="form-control" name="village_id" placeholder="Enter Village">
                        </div>
                        <div class="form-group">
                            <label for="website">Website <i class="text-danger">*</i></label>
                            <input required type="text" class="form-control" name="website" placeholder="Enter Website">
                        </div>
                        <div class="form-group">
                            <label for="email">Email <i class="text-danger">*</i></label>
                            <input required type="email" class="form-control" name="email" placeholder="Enter Email">
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" id="load">Save changes</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <!-- update data -->
    <div class="modal fade" id="edit-company-profile" aria-modal="true" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="" id="update-company-profile">
                    <div class="modal-header">
                        <h4 class="modal-title">Edit Company Profile</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" class="form-control" name="id" placeholder="ID Comp">
                        <div class="form-group">
                            <label for="name">Name <i class="text-danger">*</i></label>
                            <input required type="text" class="form-control" name="name" placeholder="Enter Company Name">
                        </div>
                        <div class="form-group">
                            <label for="bussiness_type">Bussiness Type <i class="text-danger">*</i></label>
                            <input required type="text" class="form-control" name="bussiness_type" placeholder="Enter Bussiness Type">
                        </div>
                        <div class="form-group">
                            <label for="npwp_number">NPWP Number <i class="text-danger">*</i></label>
                            <input required type="text" class="form-control" name="npwp_number" placeholder="Enter NPWP Number">
                        </div>
                        <div class="form-group">
                            <label for="address_1">Adress 1 <i class="text-danger">*</i></label>
                            <input required type="text" class="form-control" name="address_1" placeholder="Enter Adress 1">
                        </div>
                        <div class="form-group">
                            <label for="address_2">Address 2</i></label>
                            <input type="text" class="form-control" name="address_2" placeholder="Enter Address 2">
                        </div>
                        <div class="form-group">
                            <label for="telepon_1">Telepon 1 <i class="text-danger">*</i></label>
                            <input required type="text" class="form-control" name="telepon_1" placeholder="Enter Telepon 1">
                        </div>
                        <div class="form-group">
                            <label for="telepon_2">Telepon 2 </label>
                            <input type="text" class="form-control" name="telepon_2" placeholder="Enter Telepon 2">
                        </div>
                        <div class="form-group">
                            <label for="fax">Fax <i class="text-danger">*</i></label>
                            <input required type="text" class="form-control" name="fax" placeholder="Enter Fax">
                        </div>
                        <div class="form-group">
                            <label for="bank_name">Bank Name <i class="text-danger">*</i></label>
                            <input required type="text" class="form-control" name="bank_name" placeholder="Enter Bank Name">
                        </div>
                        <div class="form-group">
                            <label for="bank_rekening_number">Bank Account Number <i class="text-danger">*</i></label>
                            <input required type="text" class="form-control" name="bank_rekening_number" placeholder="Enter Bank Rekening Number">
                        </div>
                        <div class="form-group">
                            <label for="bank_owner">Bank Owner <i class="text-danger">*</i></label>
                            <input required type="text" class="form-control" name="bank_owner" placeholder="Enter Bank Owner">
                        </div>
                        <div class="form-group">
                            <label for="country_id">Country <i class="text-danger">*</i></label>
                            <input required type="text" class="form-control" name="country_id" placeholder="Enter Country">
                        </div>
                        <div class="form-group">
                            <label for="province_id">Province <i class="text-danger">*</i></label>
                            <input required type="text" class="form-control" name="province_id" placeholder="Enter Province">
                        </div>
                        <div class="form-group">
                            <label for="city_id">City <i class="text-danger">*</i></label>
                            <input required type="text" class="form-control" name="city_id" placeholder="Enter City">
                        </div>
                        <div class="form-group">
                            <label for="district_id">District</i></label>
                            <input type="text" class="form-control" name="district_id" placeholder="Enter District">
                        </div>
                        <div class="form-group">
                            <label for="village_id">Village</i></label>
                            <input type="text" class="form-control" name="village_id" placeholder="Enter Village">
                        </div>
                        <div class="form-group">
                            <label for="website">Website <i class="text-danger">*</i></label>
                            <input required type="text" class="form-control" name="website" placeholder="Enter Website">
                        </div>
                        <div class="form-group">
                            <label for="email">Email <i class="text-danger">*</i></label>
                            <input required type="text" class="form-control" name="email" placeholder="Enter Email">
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection


@once

    @push('page_scripts')
        <script src="{{ asset('js/dataTables/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('js/dataTables/dataTables.bootstrap4.min.js') }}"></script>
        <script src="https://adminlte.io/themes/v3/dist/js/adminlte.min.js"></script>
        <script src="https://adminlte.io/themes/v3/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
        {{-- <script src="{{ asset('js/app.js') }}" defer></script> --}}
        <script src="{{ asset('js/core.js') }}" defer></script>
        <script src="{{ asset('js/companyprofile.js') }}" defer></script>
    @endpush

@endonce
