@extends('layouts.app')

@once
@push('page_css')
<link rel="stylesheet" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css">
@endpush
@endonce

@section('third_party_scripts')
<script src="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.js"></script>
@endsection

@section('content')
    <div class="container-fluid pt-4">
        <h2 class="text-black-10">Kelompok Barang
        </h2>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Data Kelompok Barang
                        </h3>

                        <div class="card-tools">
                            <div class="input-group input-group-sm">
                                <button class="mr-2 float-right btn btn-primary btn-sm" data-toggle="modal"
                                    data-target="#add-item-group"><i class="fas fa-plus"></i> Add</button>
                                <button class="mr-2 float-right btn btn-danger btn-sm" id="deleteAllSelectedRecords"><i class="fas fa-trash"></i>
                                    Delete Selected</button>
                                
                            </div>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body table-responsive p-0 table-hover" >
                        <table class="table table-head-fixed text-nowrap" id="barang">
                            <thead>
                                <tr>
                                    <th><input type="checkbox" id="chkCheckAll" /></th>
                                    <th>ID</th>
                                    <th>Group Name</th>
                                    <th>Status</th>
                                    <th>Created at</th>
                                    <th>Updated at</th>
                                    <th>Modify</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($item_group as $data)
                                    <tr id="sid{{ $data->id }}">
                                        <td><input type="checkbox" name="ids" class="checkBoxClass" value="{{ $data->id }}" /></td>
                                        <td>{{ $data->id }}</td>
                                        <td>{{ $data->name }}</td>
                                        <td>{{ $data->status }}</td>
                                        <td>{{ $data->created_at }}</td>
                                        <td>{{ $data->updated_at }}</td>
                                        <td><a href='#' data-id="{{ $data->id }}" class="btn btn-success btn-sm btn-edit"><i class="nav-icon fas fa-edit"></i></a>
                                            <a href="{{ route('item-group-delete',['id'=>$data->id]) }}" onclick="return confirm('Yakin Hapus data')" class="btn btn-danger btn-sm">
                                                    <i class="nav-icon fas fa-trash"></i>
                                                  </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
    </div>

    <!-- Modal Add -->
    <div class="modal fade" id="add-item-group" aria-modal="true" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add Item Group</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form action="{{ route('add-item-group') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" name="name" placeholder="Enter name">
                    </div>
                    <div class="form-group">
                        <label for="item_code">Status</label>
                        <input type="text" class="form-control" name="status" placeholder="Status">
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <!-- Modal Update -->
    <div class="modal fade" id="modal-edit" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Item</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form action="{{ route('add-item') }}" method="POST" id="form-edit" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary btn-update">Save changes</button>
                </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection

@once
@push('page_scripts')
<script type="text/javascript" src="{{ asset('js/itemgroup/itemgroup.js') }}">
</script>
@endpush
@endonce