@extends('layouts.app')

@section('content')

    <div class="container-fluid pt-4">
        <h2 class="text-black-10">Role Master
        </h2>

        <div class="row">
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box">
                    <button style="border: none" name="total" class="info-box-icon bg-primary elevation-1"><i class="fas fa-user-cog"></i></button>

                    <div class="info-box-content">
                        <span class="info-box-text">Total</span>
                        <span class="info-box-number" name="all">0
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box mb-3">
                    <button style="border: none" name="active" class="info-box-icon bg-success elevation-1"><i class="fas fa-thumbs-up"></i></button>

                    <div class="info-box-content">
                        <span class="info-box-text">Aktif</span>
                        <span class="info-box-number" name="active">0</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->

            <!-- fix for small devices only -->
            <div class="clearfix hidden-md-up"></div>

            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box mb-3">
                    <button style="border: none" name="inactive" class="info-box-icon bg-danger elevation-1"><i class="fas fa-thumbs-down"></i></button>

                    <div class="info-box-content">
                        <span class="info-box-text">Tidak Aktif</span>
                        <span class="info-box-number" name="inactive">0</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box mb-3">
                    <button style="border: none" name="trashed" class="info-box-icon bg-info elevation-1"><i class="fas fa-recycle"></i></button>

                    <div class="info-box-content">
                        <span class="info-box-text">Recycle</span>
                        <span class="info-box-number" name="trashed">0</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Data Role
                        </h3>

                        <div class="card-tools">
                            <div class="input-group input-group-sm">
                                <button class="mr-2 float-right btn btn-primary btn-sm" data-toggle="modal"
                                    data-target="#add-role"><i class="fas fa-plus"></i> Add</button>
                                <button class="mr-2 float-right btn btn-danger btn-sm" id="delete"><i
                                        class="fas fa-trash"></i>
                                    Delete</button>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body table-responsive p-2">
                        <table class="table table-head-fixed text-nowrap" id="table">
                            <thead>
                                <tr>
                                    <th>
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox" id="customCheckbox"
                                                onchange="checkbox_all(this)">
                                            <label for="customCheckbox" class="custom-control-label"></label>
                                        </div>
                                    </th>
                                    <th>Role</th>
                                    <th>Description</th>
                                    <th>Status</th>
                                    <th>Created At</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
    </div>

    <div class="modal fade" id="add-role" aria-modal="true" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="" id="insert-role">
                    <div class="modal-header">
                        <h4 class="modal-title">Add Role</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" name="name" placeholder="Enter name role">
                        </div>
                        <div class="form-group">
                            <label for="id_role">ID Role</label>
                            <input type="text" class="form-control" name="id_role" placeholder="ID Role">
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea class="form-control" name="description"
                                placeholder="Enter description role"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="edit-role" aria-modal="true" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="" id="update-role">
                    <div class="modal-header">
                        <h4 class="modal-title">Add Role</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" name="name" placeholder="Enter name role">
                        </div>
                        <div class="form-group">
                            <label for="id_role">ID Role</label>
                            <input type="text" class="form-control" name="id_role" placeholder="ID Role">
                            <input type="hidden" class="form-control" name="id" placeholder="ID Role">
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea class="form-control" name="description"
                                placeholder="Enter description role"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection


@once

    @push('page_scripts')
        <script src="https://cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
        <script src="https://adminlte.io/themes/v3/dist/js/adminlte.min.js"></script>
        <script src="https://adminlte.io/themes/v3/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
        {{-- <script src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap4.min.js"></script> --}}
        {{-- <script src="{{ asset('js/app.js') }}" defer></script> --}}
        <script src="{{ asset('js/core.js') }}" defer></script>
        <script src="{{ asset('js/role.js') }}" defer></script>
    @endpush

@endonce
