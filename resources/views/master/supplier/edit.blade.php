<div class="form-group">
    <input type="hidden" name="id" value="{{ $supplier->id }}" id="id_data">
    <label for="name">Tipe Supplier</label>
    <select class="form-control" name="type_id">
    @foreach($suppliertype as $suppliertype)
        <option value="{{ $suppliertype->id }}" <?php if($supplier->type_id == $suppliertype->id){ echo 'selected';} ?>>{{ $suppliertype->type_name }}</option>
    @endforeach
    </select>
</div>
<div class="form-group">
    <label for="nama_supplier">Nama Supplier</label>
    <input type="text" class="form-control" name="nama_supplier" placeholder="Tambah Nama Supplier" value="{{ $supplier->nama_supplier }}">
</div>
<div class="form-group">
    <label for="no_telepon">No Telepon</label>
    <input type="text" class="form-control" name="no_telepon" placeholder="Tambah No Telepon" value="{{ $supplier->no_telepon }}">
</div>
<div class="form-group">
    <label for="alamat">Alamat</label>
    <textarea class="form-control" id="alamat" rows="3" name="alamat">{{ $supplier->alamat }}</textarea>
</div>