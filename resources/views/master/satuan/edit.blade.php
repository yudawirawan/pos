<div class="form-group">
    <input type="hidden" name="id" value="{{ $data->id }}" id="id_data">
    <label for="name">Nama Satuan</label>
    <input type="text" class="form-control" name="name_unit" placeholder="Tambah nama unit" value="{{ $data->name_unit }}" required>
</div>
<div class="form-group">
    <label for="id_role">Kode</label>
    <input type="text" class="form-control" name="singkatan" placeholder="Kode" value="{{ $data->singkatan }}" required>
</div>
<div class="form-group">
    <label for="exampleFormControlSelect2">Pilih Kategori</label>
        <select class="form-control" name="status" id="exampleFormControlSelect2">
            <option value="Aktif" <?php if($data->status == 'Aktif'){ echo 'selected';} ?>>Aktif</option>
            <option value="Tidak Aktif" <?php if($data->status == 'Tidak Aktif'){ echo 'selected';} ?>>Tidak Aktif</option>
        </select>
</div>