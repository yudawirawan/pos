@extends('layouts.app')

@once
@push('page_css')
<link rel="stylesheet" href="{{ asset('css/dataTables/dataTables.bootstrap4.min.css') }}">
@endpush
@endonce

@section('content')

    <div class="container-fluid pt-4">
        <h2 class="text-black-10">Production Detail
        </h2>

        <div class="row">
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box">
                    <button style="border: none" name="total" class="info-box-icon bg-primary elevation-1"><i class="fas fa-user-cog"></i></button>

                    <div class="info-box-content">
                        <span class="info-box-text">Total</span>
                        <span class="info-box-number" name="all">0
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            
            <!-- /.col -->
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box mb-3">
                    <button style="border: none" name="trashed" class="info-box-icon bg-info elevation-1"><i class="fas fa-recycle"></i></button>

                    <div class="info-box-content">
                        <span class="info-box-text">Recycle</span>
                        <span class="info-box-number" name="trashed">0</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Data Production Detail
                        </h3>

                        <div class="card-tools">
                            <div class="input-group input-group-sm">
                                <button class="mr-2 float-right btn btn-primary btn-sm" data-toggle="modal"
                                    data-target="#add-production-detail" ><i class="fas fa-plus"></i> Add</button>
                                <button class="mr-2 float-right btn btn-danger btn-sm" id="delete"><i
                                        class="fas fa-trash"></i>
                                    Delete</button>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body table-responsive p-2">
                        <table class="table table-head-fixed text-nowrap" id="table">
                            <thead>
                                <tr>
                                    <th>
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox" id="customCheckbox"
                                                onchange="checkbox_all(this)">
                                            <label for="customCheckbox" class="custom-control-label"></label>
                                        </div>
                                    </th>
                                    <th>Item</th>
                                    <th>Price</th>
                                    <th>Quantity</th>
                                    <th>Amount Price</th>
                                    <th>Created By</th>
                                    <th>Created At</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
    </div>

    <!-- insert data -->
    <div class="modal fade" id="add-production-detail" aria-modal="true" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="" id="insert-production-detail">
                    <div class="modal-header">
                        <h4 class="modal-title">Add Production Detail</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <table class="col-sm-12" id="dynamicAddRemove">
                        <input type="hidden" name="moreFields[0][created_by]" value="{{$user_id}}">
                        <div class="form-group">
                            <label for="item_id">Item <i class="text-danger">*</i></label>
                            <select required class="form-control" name="moreFields[0][item_id]">
                                    <option value="" >== Select Item ==</option>
                                @foreach ($item as $i)
                                    <option value="{{$i->id}}">
                                    {{$i->id}} - {{$i->item_name}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="price">Price <i class="text-danger">*</i></label>
                            <input required type="number" class="form-control" name="moreFields[0][price]" placeholder="Enter quantity">
                        </div>
                        <div class="form-group">
                            <label for="qty">Quantity <i class="text-danger">*</i></label>
                            <input required type="number" class="form-control" name="moreFields[0][qty]" placeholder="Enter quantity">
                        </div>
                        <div class="form-group">
                            <label for="amount_price">Amount Price</label>
                            <input type="number" class="form-control" name="moreFields[0][amount_price]" placeholder="Enter Amount Price">
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea class="form-control" name="moreFields[0][description]" placeholder="Enter Description"></textarea>
                        </div>
                        <div class="justify-content-between">
                            <button type="button" name="add" id="add-btn" class="btn btn-success col-sm-12">Add More Fields</button>
                        </div>
                        </table>
                    </div><br/>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <!-- update data -->
    <div class="modal fade" id="edit-production-detail" aria-modal="true" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="" id="update-production-detail">
                    @csrf
                    <div class="modal-header">
                        <h4 class="modal-title">Edit Production Detail</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" class="form-control" name="id" placeholder="ID production detail">
                        <div class="form-group">
                            <label for="item_id">Item <i class="text-danger">*</i></label>
                            <select required class="form-control" name="item_id">
                                    <option value="" >== Select Item ==</option>
                                @foreach ($item as $i)
                                    <option value="{{$i->id}}">
                                    {{$i->id}} - {{$i->item_name}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="price">Price <i class="text-danger">*</i></label>
                            <input required type="number" class="form-control" name="price" placeholder="Enter quantity">
                        </div>
                        <div class="form-group">
                            <label for="qty">Quantity <i class="text-danger">*</i></label>
                            <input required type="number" class="form-control" name="qty" placeholder="Enter quantity">
                        </div>
                        <div class="form-group">
                            <label for="amount_price">Amount Price</label>
                            <input type="number" class="form-control" name="amount_price" placeholder="Enter Amount Price">
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea class="form-control" name="description" placeholder="Enter Description"></textarea>
                        </div>
                    </div>
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary" id="updating">Save changes</button>
                        </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection


@once

    @push('page_scripts')
        <script src="{{ asset('js/dataTables/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('js/dataTables/dataTables.bootstrap4.min.js') }}"></script>
        <script src="https://adminlte.io/themes/v3/dist/js/adminlte.min.js"></script>
        <script src="https://adminlte.io/themes/v3/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
        {{-- <script src="{{ asset('js/app.js') }}" defer></script> --}}
        <script src="{{ asset('js/core.js') }}" defer></script>
        <script src="{{ asset('js/productiondetail.js') }}" defer></script>
        <script type="text/javascript">
            var i = 0;
            $("#add-btn").click(function(){
            ++i;
            $("#dynamicAddRemove").append('<div id="addfield"><br/><hr width="100%" /><br/><input type="hidden" name="moreFields['+i+'][created_by]" value="{{$user_id}}"><div class="form-group"><label for="item_id">Item <i class="text-danger">*</i></label><select required class="form-control" name="moreFields['+i+'][item_id]"><option value="" >== Select Item ==</option>@foreach ($item as $i)<option value="{{$i->id}}">{{$i->id}} - {{$i->item_name}}</option>@endforeach</select></div><div class="form-group"><label for="price">Price <i class="text-danger">*</i></label><input required type="number" class="form-control" name="moreFields['+i+'][price]" placeholder="Enter quantity"></div><div class="form-group"><label for="qty">Quantity <i class="text-danger">*</i></label><input required type="number" class="form-control" name="moreFields['+i+'][qty]" placeholder="Enter quantity"></div><div class="form-group"><label for="amount_price">Amount Price</label><input type="number" class="form-control" name="moreFields['+i+'][amount_price]" placeholder="Enter Amount Price"></div><div class="form-group"><label for="description">Description</label><textarea class="form-control" name="moreFields['+i+'][description]" placeholder="Enter Description"></textarea></div><button type="button" class="btn btn-danger remove-tr col-sm-12">Remove Fields</button></div>');
            });
            $(document).on('click', '.remove-tr', function(){  
            $(this).parents('#addfield').remove();
            });  
        </script>
    @endpush

@endonce
