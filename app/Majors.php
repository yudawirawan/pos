<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Majors extends Model
{
    use softDeletes;
    protected $fillable = ['major_name','deleted_at'];
    protected $dates = ['deleted_at'];
}
