<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Satuan;

class Bincard extends Model
{
    //
    use SoftDeletes;
    protected $fillable = ['invoice', 'bintype', 'item_id', 'qty', 'price', 'amount_price', 'uom_code'
                            , 'status', 'memo', 'created_by', 'deleted_at'];
    protected $dates = ['deleted_at'];

    public function uom(){
        return $this->belongsTo(Satuan::class,'uom_code','id');
    }
}
