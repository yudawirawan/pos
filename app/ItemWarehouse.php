<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItemWarehouse extends Model
{
    protected $table = 'item_warehouses';
    protected $fillable = [
        'item_id', 'qty', 'uom_code', 'item_group_id', 'memo', 'created_by', 'created_at', 'deleted_at'
    ];
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    
    public function user()
    {
      return $this->belongsTo(User::class, 'created_by', 'id');
    }
    
    public function item()
    {
      return $this->belongsTo(Item::class, 'item_id', 'id');
    }

    public function satuan()
    {
      return $this->belongsTo(Satuan::class, 'uom_code', 'id');
    }

    public function item_group()
    {
      return $this->belongsTo(Itemgroup::class, 'item_group_id', 'id');
    }
}
