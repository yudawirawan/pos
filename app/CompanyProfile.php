<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompanyProfile extends Model
{
    protected $table = 'company_profiles';
    protected $fillable = [
        'name', 'bussiness_type', 'npwp_number', 'address_1', 'address_2', 'telepon_1', 'telepon_2', 'fax', 'bank_name', 'bank_rekening_number', 'bank_owner', 'country_id', 'province_id', 'city_id', 'district_id', 'village_id', 'website', 'email', 'status', 'deleted_at', 'created_by', 'created_at', 'updated_at'
    ];
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function user()
    {
      return $this->belongsTo(User::class, 'created_by', 'id');
    }
}
