<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\CompanyProfile;
use App\User;

class CompanyProfileController extends Controller
{
    public function index()
    {
        return view('master.companyProfile');
    }

    public function get()
    {
        if (empty($_GET['parm'])) {
            $data = CompanyProfile::with('user');
        } else {
            if (($_GET['parm']) == 'trashed') {
                $data = CompanyProfile::with('user')->onlyTrashed();
            } else {
                $data = CompanyProfile::with('user')->where($_GET['parm'], $_GET['value'])->get();
            }
        }

        return DataTables::of($data)
            ->addColumn('btn', function ($data) {
                if (!empty($_GET['parm']) && $_GET['parm'] == 'trashed') {
                    $btn = '<div class="btn-group">
                        <button type="button" id="restore" data-id="' . $data->id . '" class="btn btn-sm btn-info">
                            <i class="fas fa-recycle"></i>
                        </button>
                        <button type="button" id="deletePermanent" data-id="' . $data->id . '" class="btn btn-sm btn-danger">
                            <i class="fas fa-trash-alt"></i>
                        </button>
                    </div>';
                } else {
                    $btn = '<div class="btn-group">
                        <button type="button" id="edit" data-id="' . $data->id . '" class="btn btn-sm btn-warning">
                            <i class="fas fa-edit"></i>
                        </button>
                        <button type="button" id="delete" data-id="' . $data->id . '" class="btn btn-sm btn-danger">
                            <i class="fas fa-trash"></i>
                        </button>
                  </div>';
                }


                return $btn;
            })
            ->addColumn('check', function ($data) {
                return '<div class="custom-control custom-checkbox">
                <input class="custom-control-input" name="checkbox-item" value="' . $data->id . '" type="checkbox" id="customCheckbox' . $data->id . '" onchange="checkbox_this(this)">
                <label for="customCheckbox' . $data->id . '" class="custom-control-label"></label>
            </div>';
            })
            ->addColumn('status', function ($data) {
                $checked = $data->status == 1 ? 'checked' : '';
                return '<div class="custom-control custom-switch">
                <input type="checkbox" class="custom-control-input status" ' . $checked . ' id="customSwitch' . $data->id . '" value="' . $data->status . '" data-id="' . $data->id . '">
                <label class="custom-control-label" for="customSwitch' . $data->id . '"></label>
              </div>';
            })
            ->editColumn('created_at', function ($data) {
                return $data->created_at->format('m/d/Y H:i:s') ;
            })
            ->rawColumns(['btn', 'check', 'status'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function getById($id)
    {
        $data = CompanyProfile::find($id);
        return response()->json(['message' => 'query berhasil', 'status' => 'success', 'data' => $data], 200);
    }

    public function all()
    {
        $data['all']  = CompanyProfile::all()->count();
        $data['active'] = CompanyProfile::all()->where('status', 1)->count();
        $data['inactive'] = CompanyProfile::all()->where('status', 0)->count();
        $data['trashed'] = CompanyProfile::withTrashed()->where('deleted_at', '!=', null)->count();

        return response()->json(['message' => 'query telah berhasil', 'status' => 'success', 'data' => $data], 200);
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            $user_id = \Auth::user()->id;
            CompanyProfile::create([
                'name' => $request->name,
                'bussiness_type' => $request->bussiness_type,
                'npwp_number' => $request->npwp_number,
                'address_1' => $request->address_1,
                'address_2' => $request->address_2,
                'telepon_1' => $request->telepon_1,
                'telepon_2' => $request->telepon_2,
                'fax' => $request->fax,
                'bank_name' => $request->bank_name,
                'bank_rekening_number' => $request->bank_rekening_number,
                'bank_owner' => $request->bank_owner,
                'country_id' => $request->country_id,
                'province_id' => $request->province_id,
                'city_id' => $request->city_id,
                'district_id' => $request->district_id,
                'village_id' => $request->village_id,
                'website' => $request->website,
                'email' => $request->email,
                'created_by' => $user_id,
            ]);
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $data = CompanyProfile::find($request->id);
        $data->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function change(Request $request)
    {
        $status = $request->status == 1 ? '0' : 1;
        $data = CompanyProfile::find($request->id);
        $data->update(['status' => $status]);
    }

    public function delete(Request $request)
    {
        if (empty($request->parm)) {
            if (is_array($request->id)) {
                foreach ($request->id as $value) {
                    $data = CompanyProfile::where('id', $value);
                    $data->delete();
                }
            } else {
                $data = CompanyProfile::where('id', $request->id);
                $data->delete();
            }
        } else {
            if ($request->parm == 'restore') {
                if (is_array($request->id)) {
                    foreach ($request->id as $value) {
                        $data = CompanyProfile::onlyTrashed()->where('id', $value);
                        $data->restore();
                    }
                } else {
                    $data = CompanyProfile::onlyTrashed()->where('id', $request->id);
                    $data->restore();
                }
            } elseif ($request->parm == 'deletePermanent') {
                if (is_array($request->id)) {
                    foreach ($request->id as $value) {
                        $data = CompanyProfile::withTrashed()->where('id', $value);
                        $data->forceDelete();
                    }
                } else {
                    $data = CompanyProfile::withTrashed()->where('id', $request->id);
                    $data->forceDelete();
                }
            }

        }
    }
}
