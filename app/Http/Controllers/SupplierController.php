<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Supplier;
use App\SupplierType;
use Yajra\DataTables\DataTables;
use Session;

class SupplierController extends Controller
{
    public function index(){

        $supplier = DB::table('suppliers')
                    ->join('supplier_types', 'supplier_types.id', '=', 'suppliers.type_id')
                    ->select('suppliers.*', 'supplier_types.type_name as type_name')
                    ->latest()
                    ->get();

        $data = array(
            'supplier' => $supplier,
            'suppliertype' => SupplierType::all(),
        );
        
        return view('master.supplier.supplier',$data);
    }
    
    public function supplierGet()
    {
        if (empty($_GET['parm'])) {
            $data = Supplier::with('suppliertype');
        } else {
            if (($_GET['parm']) == 'trashed') {
                $data = Supplier::with('suppliertype')->onlyTrashed();
            } else {
                $data = Supplier::with('suppliertype')->where($_GET['parm'], $_GET['value'])->get();
            }
        }

        return DataTables::of($data)
            ->addColumn('btn', function ($data) {
                if (!empty($_GET['parm']) && $_GET['parm'] == 'trashed') {
                    $btn = '<div class="btn-group" role="group">
                        <button type="button" id="restore" data-id="' . $data->id . '" class="btn btn-sm btn-info">
                            <i class="fas fa-recycle"></i>
                        </button>
                        <button type="button" id="deletePermanent" data-id="' . $data->id . '" class="btn btn-sm btn-danger">
                            <i class="fas fa-trash-alt"></i>
                        </button>
                    </div>';
                } else {
                    $btn = '<div class="btn-group" role="group" >
                        <button type="button" id="edit" data-id="' . $data->id . '" class="btn btn-sm btn-warning">
                            <i class="fas fa-edit"></i>
                        </button>
                        <button type="button" id="delete" data-id="' . $data->id . '" class="btn btn-sm btn-danger">
                            <i class="fas fa-trash"></i>
                        </button>
                  </div>';
                }


                return $btn;
            })
            ->addColumn('check', function ($data) {
                return '<div class="custom-control custom-checkbox text-center">
                <input class="custom-control-input" name="checkbox-item" value="' . $data->id . '" type="checkbox" id="customCheckbox' . $data->id . '" onchange="checkbox_this(this)">
                <label for="customCheckbox' . $data->id . '" class="custom-control-label"></label>
            </div>';
            })
            
            ->addColumn('status', function ($data) {
                $checked = $data->status == 1 ? 'checked' : '';
                return '<div class="custom-control custom-switch text-center">
                <input type="checkbox" class="custom-control-input status" ' . $checked . ' id="customSwitch' . $data->id . '" value="' . $data->status . '" data-id="' . $data->id . '">
                <label class="custom-control-label" for="customSwitch' . $data->id . '"></label>
              </div>';
            })
            
            ->editColumn('created_at', function ($data) {
                return $data->created_at->format('Y/m/d | H:i:s');
            })

            ->addColumn('updated_at', function ($data) {
                return $data->updated_at->format('Y/m/d | H:i:s');
            })
            

            ->rawColumns(['btn', 'check', 'status', 'created_at', 'updated_at'])
            ->make(true);
    }

    public function supplierInsert(Request $request)
    {
        Supplier::create($request->all());

    }

    public function edit($id)
    {
        $data = array(
            'supplier' => Supplier::findOrFail($id),
            'suppliertype' => SupplierType::all(),
        );

        return view ('master.supplier.edit', $data);
    }

    public function getById($id)
    {
        $data = Supplier::find($id);
        return response()->json(['message' => 'query berhasil', 'status' => 'success', 'data' => $data], 200);
    }

    public function delete(Request $request)
    {
        if (empty($request->parm)) {
            if (is_array($request->id)) {
                foreach ($request->id as $value) {
                    $data = Supplier::where('id', $value);
                    $data->delete();
                }
            } else  {
                $data = Supplier::where('id', $request->id);
                $data->delete();
            } 
        } else {
            if ($request->parm == 'restore') {
                if (is_array($request->id)) {
                    foreach ($request->id as $value) {
                        $data = Supplier::onlyTrashed()->where('id', $value);
                        $data->restore();
                    }
                } else {
                    $data = Supplier::onlyTrashed()->where('id', $request->id);
                    $data->restore();
                }
            } else {
                if (is_array($request->id)) {
                    foreach ($request->id as $value) {
                        $data = Supplier::withTrashed()->where('id', $value);
                        $data->forceDelete();

                    }
                } else {
                    $data = Supplier::withTrashed()->where('id', $request->id);
                    $data->forceDelete();
                }
            }

        }
    }

    public function update(Request $request)
    {
        $data = Supplier::find($request->id);
        $data->update($request->all());

    }

    public function all()
    {
        $data['all']  = Supplier::all()->count();
        $data['active'] = Supplier::all()->where('status', 1)->count();
        $data['inactive'] = Supplier::all()->where('status', 0)->count();
        $data['trashed'] = Supplier::withTrashed()->where('deleted_at', '!=', null)->count();

        return response()->json(['message' => 'query telah berhasil', 'status' => 'success', 'data' => $data], 200);
    }

    public function change(Request $request)
    {
        $status = $request->status == 1 ? '0' : 1;
        $data = Supplier::find($request->id);
        $data->update(['status' => $status]);
    }
}
