<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Production;
use App\Branch;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Validator;

class ProductionController extends Controller
{
    public function index()
    {
        $user_id = \Auth::user()->id;
        $branch = Branch::all();
        return view('master.production' , compact('branch', 'user_id'));
    }

    public function get()
    {
        if (empty($_GET['parm'])) {
            $data = Production::with('user', 'branch', 'user_update');
        } else {
            if (($_GET['parm']) == 'trashed') {
                $data = Production::with('user', 'branch', 'user_update')->onlyTrashed();
            } else {
                $data = Production::with('user', 'branch', 'user_update')->where($_GET['parm'], $_GET['value'])->get();
            }
        }

        return DataTables::of($data)
            ->addColumn('btn', function ($data) {
                if (!empty($_GET['parm']) && $_GET['parm'] == 'trashed') {
                    $btn = '<div class="btn-group">
                        <button type="button" id="restore" data-id="' . $data->id . '" class="btn btn-sm btn-info">
                            <i class="fas fa-recycle"></i>
                        </button>
                        <button type="button" id="deletePermanent" data-id="' . $data->id . '" class="btn btn-sm btn-danger">
                            <i class="fas fa-trash-alt"></i>
                        </button>
                    </div>';
                } else {
                    $btn = '<div class="btn-group">
                        <button type="button" id="edit" data-id="' . $data->id . '" class="btn btn-sm btn-warning">
                            <i class="fas fa-edit"></i>
                        </button>
                        <button type="button" id="delete" data-id="' . $data->id . '" class="btn btn-sm btn-danger">
                            <i class="fas fa-trash"></i>
                        </button>
                  </div>';
                }


                return $btn;
            })
            ->addColumn('check', function ($data) {
                return '<div class="custom-control custom-checkbox">
                <input class="custom-control-input" name="checkbox-item" value="' . $data->id . '" type="checkbox" id="customCheckbox' . $data->id . '" onchange="checkbox_this(this)">
                <label for="customCheckbox' . $data->id . '" class="custom-control-label"></label>
            </div>';
            })
            ->addColumn('status', function ($data) {
                $checked = $data->status == 1 ? 'checked' : '';
                return '<div class="custom-control custom-switch">
                <input type="checkbox" class="custom-control-input status" ' . $checked . ' id="customSwitch' . $data->id . '" value="' . $data->status . '" data-id="' . $data->id . '">
                <label class="custom-control-label" for="customSwitch' . $data->id . '"></label>
              </div>';
            })
            ->editColumn('created_at', function ($data) {
                return $data->created_at->format('m/d/Y H:i:s') ;
            })
            ->rawColumns(['btn', 'check', 'status'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function getById($id)
    {
        $data = Production::find($id);
        return response()->json(['message' => 'query berhasil', 'status' => 'success', 'data' => $data], 200);
    }

    public function all()
    {
        $data['all']  = Production::all()->count();
        $data['active'] = Production::all()->where('status', 1)->count();
        $data['inactive'] = Production::all()->where('status', 0)->count();
        $data['trashed'] = Production::withTrashed()->where('deleted_at', '!=', null)->count();

        return response()->json(['message' => 'query telah berhasil', 'status' => 'success', 'data' => $data], 200);
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            // $user_id = \Auth::user()->id;
            // Production::create([
            //     'invoice' => $request->invoice,
            //     'branch_id' => $request->branch_id,
            //     'description' => $request->description,
            //     'amount' => $request->amount,
            //     'status' => '1',
            //     'date' => $request->date,
            //     'created_by' => $user_id,
            // ]);
        
        $request->validate([
            'moreFields.*.invoice' => 'required',
            'moreFields.*.branch_id' => 'required',
            'moreFields.*.description' => 'required',
            'moreFields.*.amount' => 'required',
            'moreFields.*.date' => 'required',
            'moreFields.*.created_by',
            'moreFields.*.updated_by',
        ]);

        foreach ($request->moreFields as $key => $value) {
            Production::create($value);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $user_id = \Auth::user()->id;
        $data = Production::find($request->id);
        $data->update([
                'invoice' => $request->invoice,
                'branch_id' => $request->branch_id,
                'description' => $request->description,
                'amount' => $request->amount,
                'date' => $request->date,
                'updated_by' => $user_id,
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function change(Request $request)
    {
        $status = $request->status == 1 ? '0' : 1;
        $data = Production::find($request->id);
        $data->update(['status' => $status]);
    }

    public function delete(Request $request)
    {
        if (empty($request->parm)) {
            if (is_array($request->id)) {
                foreach ($request->id as $value) {
                    $data = Production::where('id', $value);
                    $data->delete();
                }
            } else {
                $data = Production::where('id', $request->id);
                $data->delete();
            }
        } else {
            if ($request->parm == 'restore') {
                if (is_array($request->id)) {
                    foreach ($request->id as $value) {
                        $data = Production::onlyTrashed()->where('id', $value);
                        $data->restore();
                    }
                } else {
                    $data = Production::onlyTrashed()->where('id', $request->id);
                    $data->restore();
                }
            } elseif ($request->parm == 'deletePermanent') {
                if (is_array($request->id)) {
                    foreach ($request->id as $value) {
                        $data = Production::withTrashed()->where('id', $value);
                        $data->forceDelete();
                    }
                } else {
                    $data = Production::withTrashed()->where('id', $request->id);
                    $data->forceDelete();
                }
            }

        }
    }
}
