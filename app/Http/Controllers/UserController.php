<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\DataTables;

class UserController extends Controller
{
    //

    public function index()
    {
        return view('master.user');
    }

    public function get()
    {
        if (empty($_GET['parm'])) {
            $data = User::all();
        } else {
            if (($_GET['parm']) == 'trashed') {
                $data = User::onlyTrashed();
            } else {
                $data = User::where($_GET['parm'], $_GET['value'])->get();
            }
        }

        return DataTables::of($data)
            ->addColumn('btn', function ($data) {
                if (!empty($_GET['parm']) && $_GET['parm'] == 'trashed') {
                    $btn = '<div class="btn-group">
                        <button type="button" id="restore" data-id="' . $data->id . '" class="btn btn-sm btn-info">
                            <i class="fas fa-recycle"></i>
                        </button>
                        <button type="button" id="deletePermanent" data-id="' . $data->id . '" class="btn btn-sm btn-danger">
                            <i class="fas fa-trash-alt"></i>
                        </button>
                    </div>';
                } else {
                    $btn = '<div class="btn-group">
                        <button type="button" id="edit" data-id="' . $data->id . '" class="btn btn-sm btn-warning">
                            <i class="fas fa-edit"></i>
                        </button>
                        <button type="button" id="delete" data-id="' . $data->id . '" class="btn btn-sm btn-danger">
                            <i class="fas fa-trash"></i>
                        </button>
                  </div>';
                }


                return $btn;
            })
            ->addColumn('check', function ($data) {
                return '<div class="custom-control custom-checkbox">
                <input class="custom-control-input" name="checkbox-item" value="' . $data->id . '" type="checkbox" id="customCheckbox' . $data->id . '" onchange="checkbox_this(this)">
                <label for="customCheckbox' . $data->id . '" class="custom-control-label"></label>
            </div>';
            })
            ->addColumn('status', function ($data) {
                $checked = $data->status == 1 ? 'checked' : '';
                return '<div class="custom-control custom-switch">
                <input type="checkbox" class="custom-control-input status" ' . $checked . ' id="customSwitch' . $data->id . '" value="' . $data->status . '" data-id="' . $data->id . '">
                <label class="custom-control-label" for="customSwitch' . $data->id . '"></label>
              </div>';
            })
            ->rawColumns(['btn', 'check', 'status'])
            ->make(true);
    }

    public function getById($id)
    {
        $data = User::find($id);
        return response()->json(['message' => 'query berhasil', 'status' => 'success', 'data' => $data], 200);
    }

    protected function insert(Request $request)
    {
        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        return response()->json(['message' => 'data berhasil ditambahkan', 'status' => 'success'], 200);
    }


    public function delete(Request $request)
    {
        if (empty($request->parm)) {
            if (is_array($request->id)) {
                foreach ($request->id as $value) {
                    $data = User::where('id', $value);
                    $data->delete();
                }
            } else {
                $data = User::where('id', $request->id);
                $data->delete();
            }
        } else {

        }
    }

    public function update(Request $request)
    {
        $data = User::find($request->id);
        if (strlen($request->pass) > 0) {
            $request->request->add(['password' => Hash::make($request->pass)]);
            $data->update($request->all());
            return response()->json(['message' => 'User & password berhasil diperbaharui', 'status' => 'success'], 200);
        }else
        $data->update([
            'name' => $request->name,
            'email' => $request->email,
        ]);
        return response()->json(['message' => 'User berhasil diperbaharui', 'status' => 'success'], 200);
    }


    public function all()
    {
        $data['all']  = User::all()->count();
        $data['active'] = User::all()->where('status', 1)->count();
        $data['inactive'] = User::all()->where('status', 0)->count();

        return response()->json(['message' => 'query telah berhasil', 'status' => 'success', 'data' => $data], 200);
    }


    public function change(Request $request)
    {
        $status = $request->status == 1 ? '0' : '1';
        $data = User::find($request->id);
        $data->update(['status' => $status]);
    }


}
