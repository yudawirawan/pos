<?php

namespace App\Http\Controllers;

use App\Bincard;
use App\Satuan;
use Illuminate\Http\Request;
use DataTables;

class BincardController extends Controller
{
    public function index(Request $request)
    {
        $satuan = Satuan::all();
        return view('master.bincard.bincard', compact('satuan'));
    }

    public function customerGet()
    {
        if (empty($_GET['parm'])) {
            $data = Bincard::with('uom')->latest()->get();
        } else {
            if (($_GET['parm']) == 'trashed') {
                $data = Bincard::with('uom')->latest()->onlyTrashed();
            } else {
                $data = Bincard::with('uom')->latest()->where($_GET['parm'], $_GET['value'])->get();
            }
        }

        return DataTables::of($data)
            ->addColumn('btn', function ($data) {
                if (!empty($_GET['parm']) && $_GET['parm'] == 'trashed') {
                    $btn = '<div class="btn-group">
                        <button type="button" id="restore" data-id="' . $data->id . '" class="btn btn-sm btn-info">
                            <i class="fas fa-recycle"></i>
                        </button>
                        <button type="button" id="deletePermanent" data-id="' . $data->id . '" class="btn btn-sm btn-danger">
                            <i class="fas fa-trash-alt"></i>
                        </button>
                    </div>';
                } else {
                    $btn = '<div class="btn-group text-center">
                        <a href="javascript:void(0)" type="button" id="edit" data-id="' . $data->id . '" class="btn btn-sm btn-warning editCustomer">
                            <i class="fas fa-edit"></i>
                        </a>
                        <button type="button" id="delete" data-id="' . $data->id . '" class="btn btn-sm btn-danger">
                            <i class="fas fa-trash"></i>
                        </button>
                  </div>';
                }


                return $btn;
            })
            ->addColumn('check', function ($data) {
                return '<div class="custom-control custom-checkbox text-center">
                <input class="custom-control-input" name="checkbox-item" value="' . $data->id . '" type="checkbox" id="customCheckbox' . $data->id . '" onchange="checkbox_this(this)">
                <label for="customCheckbox' . $data->id . '" class="custom-control-label"></label>
            </div>';
            })
            // ->addColumn('photo', function ($data) {
            //     $url=asset("storage/$data->photo");
            //     return '<img src='.$url.'style="max-width: 50px; max-height: 37px;" align="center"; class="img-fluid rounded-start" alt="..." />';
            // })
            ->addColumn('status', function ($data) {
                $checked = $data->status == 1 ? 'checked' : '';
                return '<div class="custom-control custom-switch text-center">
                <input type="checkbox" class="custom-control-input status" ' . $checked . ' id="customSwitch' . $data->id . '" value="' . $data->status . '" data-id="' . $data->id . '">
                <label class="custom-control-label" for="customSwitch' . $data->id . '"></label>
              </div>';
            })
            
            ->rawColumns(['btn', 'check', 'status'])
            ->make(true);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Bincard::updateOrCreate(['id' => $request->Bincard_id],
                ['invoice' => $request->invoice, 
                 'bintype' => $request->bintype,
                 'item_id' => $request->item_id,
                 'qty' => $request->qty,
                 'price' => $request->price,
                 'amount_price' => $request->amount_price,
                 'uom_code' => $request->uom_code,
                 'status' => 1,
                 'memo' => $request->memo,

                ]);        
        return response()->json(['success'=>'Bincard saved successfully!']);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Bincard  $Customer
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Bincard = Bincard::findOrFail($id);
        return response()->json($Bincard);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Bincard  $Customer
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {

        if (empty($request->parm)) {
            if (is_array($request->id)) {
                foreach ($request->id as $value) {
                    $data = Bincard::where('id', $value);
                    $data->delete();
                }
            } else {
                $data = Bincard::where('id', $request->id);
                $data->delete();
            }
        } else {
            if ($request->parm == 'restore') {
                if (is_array($request->id)) {
                    foreach ($request->id as $value) {
                        $data = Bincard::onlyTrashed()->where('id', $value);
                        $data->restore();
                    }
                } else {
                    $data = Bincard::onlyTrashed()->where('id', $request->id);
                    $data->restore();
                }
            } else {
                if (is_array($request->id)) {
                    foreach ($request->id as $value) {
                        $data = Bincard::withTrashed()->where('id', $value);
                        $data->forceDelete();
                    }
                } else {
                    $data = Bincard::withTrashed()->where('id', $request->id);
                    $data->forceDelete();
                }
            }
        }
    }

    public function change(Request $request)
    {
        $status = $request->status == 1 ? '0' : 1;
        $data = Bincard::find($request->id);
        $data->update(['status' => $status]);
    }

    public function all()
    {
        $data['all']  = Bincard::all()->count();
        $data['active'] = Bincard::all()->where('status', 1)->count();
        $data['inactive'] = Bincard::all()->where('status', 0)->count();
        $data['trashed'] = Bincard::withTrashed()->where('deleted_at', '!=', null)->count();

        return response()->json(['message' => 'query telah berhasil', 'status' => 'success', 'data' => $data], 200);
    }
}
