<?php

namespace App\Http\Controllers;

use App\ItemPrice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use App\User;

class ItemPriceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $item = DB::table('master_item')->get();
        $branch = DB::table('branches')->get();

        return view('master.itemprice', compact('item', 'branch'));
    }

    public function get()
    {
        if (empty($_GET['parm'])) {
            $data = ItemPrice::with('user', 'item', 'branch');
        } else {
            if (($_GET['parm']) == 'trashed') {
                $data = ItemPrice::with('user', 'item', 'branch')->onlyTrashed();
            } else {
                $data = ItemPrice::with('user', 'item', 'branch')->where($_GET['parm'], $_GET['value'])->get();
            }
        }

        return DataTables::of($data)
            ->addColumn('btn', function ($data) {
                if (!empty($_GET['parm']) && $_GET['parm'] == 'trashed') {
                    $btn = '<div class="btn-group">
                        <button type="button" id="restore" data-id="' . $data->id . '" class="btn btn-sm btn-info">
                            <i class="fas fa-recycle"></i>
                        </button>
                        <button type="button" id="deletePermanent" data-id="' . $data->id . '" class="btn btn-sm btn-danger">
                            <i class="fas fa-trash-alt"></i>
                        </button>
                    </div>';
                } else {
                    $btn = '<div class="btn-group">
                        <button type="button" id="edit" data-id="' . $data->id . '" class="btn btn-sm btn-warning">
                            <i class="fas fa-edit"></i>
                        </button>
                        <button type="button" id="delete" data-id="' . $data->id . '" class="btn btn-sm btn-danger">
                            <i class="fas fa-trash"></i>
                        </button>
                  </div>';
                }


                return $btn;
            })
            ->addColumn('check', function ($data) {
                return '<div class="custom-control custom-checkbox">
                <input class="custom-control-input" name="checkbox-item" value="' . $data->id . '" type="checkbox" id="customCheckbox' . $data->id . '" onchange="checkbox_this(this)">
                <label for="customCheckbox' . $data->id . '" class="custom-control-label"></label>
            </div>';
            })
            ->addColumn('status', function ($data) {
                $checked = $data->status == 1 ? 'checked' : '';
                return '<div class="custom-control custom-switch">
                <input type="checkbox" class="custom-control-input status" ' . $checked . ' id="customSwitch' . $data->id . '" value="' . $data->status . '" data-id="' . $data->id . '">
                <label class="custom-control-label" for="customSwitch' . $data->id . '"></label>
              </div>';
            })
            ->editColumn('created_at', function ($data) {
                return $data->created_at->format('m/d/Y H:i:s') ;
            })
            ->rawColumns(['btn', 'check', 'status'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function getById($id)
    {
        $data = ItemPrice::find($id);
        return response()->json(['message' => 'query berhasil', 'status' => 'success', 'data' => $data], 200);
    }

    public function all()
    {
        $data['all']  = ItemPrice::all()->count();
        $data['active'] = ItemPrice::all()->where('status', 1)->count();
        $data['inactive'] = ItemPrice::all()->where('status', 0)->count();
        $data['trashed'] = ItemPrice::withTrashed()->where('deleted_at', '!=', null)->count();

        return response()->json(['message' => 'query telah berhasil', 'status' => 'success', 'data' => $data], 200);
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            $user_id = \Auth::user()->id;
            ItemPrice::create([
                'item_id' => $request->item_id,
                'branch_id' => $request->branch_id,
                'current_price' => $request->current_price,
                'discount_percent' => $request->discount_percent,
                'discount' => '100',
                'discount_unit' => '1',
                'discount_min' => $request->discount_min,
                'date_discount' => $request->date_discount,
                'date_end_discount' => $request->date_end_discount,
                'status' => '1',
                'created_by' => $user_id,
            ]);
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $data = ItemPrice::find($request->id);
        // $data->update($request->all());

        $data->update([
                'item_id' => $request->item_id,
                'branch_id' => $request->branch_id,
                'current_price' => $request->current_price,
                'discount_percent' => $request->discount_percent,
                'discount' => $request->discount_percent*$request->current_price/100,
                'discount_min' => $request->discount_min,
                'date_discount' => $request->date_discount,
                'date_end_discount' => $request->date_end_discount,
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function change(Request $request)
    {
        $status = $request->status == 1 ? '0' : 1;
        $data = ItemPrice::find($request->id);
        $data->update(['status' => $status]);
    }

    public function delete(Request $request)
    {
        if (empty($request->parm)) {
            if (is_array($request->id)) {
                foreach ($request->id as $value) {
                    $data = ItemPrice::where('id', $value);
                    $data->delete();
                }
            } else {
                $data = ItemPrice::where('id', $request->id);
                $data->delete();
            }
        } else {
            if ($request->parm == 'restore') {
                if (is_array($request->id)) {
                    foreach ($request->id as $value) {
                        $data = ItemPrice::onlyTrashed()->where('id', $value);
                        $data->restore();
                    }
                } else {
                    $data = ItemPrice::onlyTrashed()->where('id', $request->id);
                    $data->restore();
                }
            } elseif ($request->parm == 'deletePermanent') {
                if (is_array($request->id)) {
                    foreach ($request->id as $value) {
                        $data = ItemPrice::withTrashed()->where('id', $value);
                        $data->forceDelete();
                    }
                } else {
                    $data = ItemPrice::withTrashed()->where('id', $request->id);
                    $data->forceDelete();
                }
            }

        }
    }

}
