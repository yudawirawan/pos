<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
    //
    use SoftDeletes;
    protected $table = 'master_role';
    protected $fillable = [
        'name', 'description', 'id_role', 'status', 'created_by', 'deleted_at'
    ];
    protected $dates = ['deleted_at'];
}
