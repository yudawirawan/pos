<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Merk extends Model
{
    protected $table = 'merk';
    protected $fillable = [
      'brand_name', 'status', 'created_at', 'updated_at', 'deleted_at'
    ];
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    
    public function item()
    {
      return $this->hasMany(Item::class);
    }
}
