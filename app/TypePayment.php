<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TypePayment extends Model
{
    //
    use softDeletes;
    protected $fillable = ['code_payment','name','deleted_at'];
    protected $dates = ['deleted_at'];
}
