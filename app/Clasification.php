<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Clasification extends Model
{
    //
    use softDeletes;
    protected $fillable = ['clasification_name', 'deleted_at'];
    protected $dates = ['deleted_at'];
}
