<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Division extends Model
{
    //
    use softDeletes;
    protected $fillable = ['division_name', 'deleted_at'];
    protected $dates = ['deleted_at'];
    
}
