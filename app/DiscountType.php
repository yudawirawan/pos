<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DiscountType extends Model
{
    protected $table = 'discount_types';
    protected $fillable = [
        'name', 'percent', 'price', 'value', 'deleted_at', 'created_by', 'created_at', 'updated_at'
    ];
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function user()
    {
      return $this->belongsTo(User::class, 'created_by', 'id');
    }
}
