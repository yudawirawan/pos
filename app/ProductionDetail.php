<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductionDetail extends Model
{
    protected $table = 'production_details';
    protected $fillable = [
      'item_id', 'price', 'qty', 'amount_price', 'description', 'created_by', 'created_at', 'updated_at', 'deleted_at'
    ];
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    
    public function user()
    {
      return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function item()
    {
      return $this->belongsTo(Item::class, 'item_id', 'id');
    }
}
