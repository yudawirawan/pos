<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Table extends Model
{
    use softDeletes;
    protected $fillable = ['number_table','name','deleted_at'];
    protected $dates = ['deleted_at'];
}
