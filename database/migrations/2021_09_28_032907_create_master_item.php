<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMasterItem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_item', function (Blueprint $table) {
            $table->increments('id');
            $table->string('item_name');
            $table->char('item_code');
            $table->integer('group_id');
            $table->integer('item_group_id');
            $table->string('photo');
            $table->string('description');
            $table->integer('brand_id');
            $table->char('stock_min');
            $table->char('stock_max');
            $table->string('consignment');
            $table->enum('status', ['1', '0']);
            $table->char('created_by');
            $table->timestamp('deleted_at')->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent()->useCurrentOnUpdate();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_item');
    }
}
